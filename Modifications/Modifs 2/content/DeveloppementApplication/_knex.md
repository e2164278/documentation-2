---
title: "Knex"
weight: 6
date: 2022-01-11T14:19:27Z
draft: true
---

### Installation de Knex
Knex est une librairie qui permet de formuler des requêtes à la bd sans avoir à construire une chaîne de caractère contenant la requêtes en langage SQL. En Knex on peut interroger la bd à l'aide de son "query builder". La requête est construite en appelant une chaîne de fonctions, chaque fonction retournant la fonction invoquée.  

La base de données cible est une bd Sql Server. Pour que Knex puisse se connecter et interagir avec cette bd, il faut installer le pilote (driver) mssql. Les deux packages sont installés comme suit:  

    npm install knex --save
    npm install mssql --save

**Informations de connexion pour la base de données:**  
Serveur: sv55.cmaisonneuve.qc.ca  
Utilisateur: 4D1EquipeXX  
Base de données: 4D1EquipeXX  

## Utilisation du query builder en Knex
Le code suivant démarre une application Express, exécute une requête à la bd, affiche le résultat à la console et retourne au client web le nombre de lignes retournées par l'exécution de la requête par la bd.  

	const express = require('express');

	const knex = require('knex')({
	  client: 'mssql',
	  connection: {
		host: 'sv55.cmaisonneuve.qc.ca',
		user: '3D1',
		password: 'Projet3689',
		database: 'college',
		options: {
		  enableArithAbort: false,
		},
	  },
	  pool: { min: 0, max: 7 },
	});

	const app = express();

	// Sélection d'une table
	// Select * from programmes
	function programmes() {
	  return knex('programmes');
	}

	app.get('/', async (requete, reponse) => {
	  try {
		const rows = await programmes();
		reponse.send(`Nombre de lignes: ${rows.length}`);
		console.log(rows);
		return null;
	  } catch (err) {
		console.log(err);
		return null;
	  }
	});

	app.listen(5000, () => {
	  console.log('Serveur en exécution');
	});

**Lorsqu'on exécute un Select sur une bd, on précise les éléments suivants:**  

* La table interrogée;
* Les colonnes à retourner (projection);
* Le critère de sélection des lignes (clause Where);
* l'ordre de tri.

On peut ajouter aussi:  

*  une jointure;
*  un regroupement et des fonctions d'agrégation.

**Sélection d'une table**  
Le code qui suit est défini dans une fonction, vous pouvez facilement le coller dans votre code et appeler la fonction en question. Pour chaque fonction, un commentaire indique l'équivalent en SQL.   
Le sélection de la table peut être fait simplement en l'indiquant en paramètre de la fonction.   

	// Sélection d'une table
	// Select * from programmes
	function programmes() {
	  return knex('programmes');
	}

**Sélection des colonnes de la table**  
Les colonnes sont spécifiées à l'aide de la fonction select()  

	// Sélection de colonnes d'une table
	// Select id as numero, nom from programmes
	function programmesIdNom() {
	  return knex('programmes')
		.select({ numero: 'id' }, 'nom');
	}

**Colonnes calculées**  
Les colonnes calculées sont spécifiées à l'aide de la fonction raw()  

	// Sélection d'une colonne calculée d'une table
	// Select id, nom, dureeTotale / 6 as [heures par session] from programmes
	function programmesDuree() {
	  return knex('programmes')
		.select('id', 'nom', knex.raw('dureeTotale / 6 as [heures par session]'));
	}

**Requête triée**  
orderBy() permet de trier en ordre ascendant ou descendant.  

	// Sélection triée
	// Select id, nom from programmes order by nom
	function programmesTriesParNom() {
	  return knex('programmes')
		.select('id', 'nom')
		.orderBy('nom');
	}

	// Tri descendant
	// Select id, nom from programmes order by nom desc
	function programmesTriesParNomDescendant() {
	  return knex('programmes')
		.select('id', 'nom')
		.orderBy('nom', 'desc');
	}

	// Deux ordres de tri
	// Select session, sigle, titre from cours order by session, sigle
	function coursTriesParSession() {
	  return knex('cours')
		.select('session', 'sigle', 'titre')
		.orderBy(['session', 'sigle']);
	}

**Critère de sélection de lignes (clause Where)**  

	// Clause where avec opérateur d'égalité
	// Select * from programmes where id = 420
	function programme420() {
	  return knex('programmes')
		.where('id', 420);
	}

	// Clause where avec opérateur de relation
	// Select code, enonce, dureeEstimee from competences where dureeEstimee > 60
	function competencesDureeEstimeePG60() {
	  return knex('competences')
		.select('code', 'enonce', 'dureeEstimee')
		.where('dureeEstimee', '>', 60);
	}

	// Clause where avec opérateurs d'égalité séparés par and
	// Select idProgramme, code, enonce, dureeEstimee from competences
	//     where idProgramme = 420 and dureeEstimee = 45
	function competences420DureeEstimee45() {
	  return knex('competences')
		.select('idProgramme', 'code', 'enonce', 'dureeEstimee')
		.where({
		  idProgramme: 420,
		  dureeEstimee: 45,
		});
	}

	// Clause where avec opérateurs quelconques séparés par or
	// Select code, enonce, dureeEstimee from competences where dureeEstimee = 60 or dureeEstimee = 75
	function competencesDureeEstimee60ou75() {
	  return knex('competences')
		.select('code', 'enonce', 'dureeEstimee')
		.where('dureeEstimee', 60).orWhere('dureeEstimee', 75);
	}

orWhere() a son équivalent and: andWhere().  
L'utilisation de cette fonction permet d'exprimer des clauses du genre:
where id > 300 and duree < 2700  

**Fonctions d'agrégat avec ou sans regroupement**  

	// Fonctions d'agrégats sans regroupement
	// Select min(dureeEstimee) as dureeMin, max(dureeEstimee) as dureeMax, count(*) as nbCompetences
	//      from competences
	function statsDuree() {
	  return knex('competences')
		.max({ dureeMax: 'dureeEstimee' })
		.min({ dureeMin: 'dureeEstimee' })
		.count({ nbCompetences: 'id' });
	}

	// Fonctions d'agrégats avec regroupement
	// Select min(dureeEstimee) as dureeMin, max(dureeEstimee) as dureeMax, count(*) as nbCompetences
	//      from competences group by idProgramme
	function statsDureeParProgramme() {
	  return knex('competences')
		.groupBy('idProgramme')
		.max({ idProgramme: 'idProgramme' })
		.max({ dureeMax: 'dureeEstimee' })
		.min({ dureeMin: 'dureeEstimee' })
		.count({ nbCompetences: 'id' });
	}

**Jointures**  

	// Jointure
	// Select programmes.id, nom, code, enonce
	//     from programmes inner join competences on programmes.id = competences.idProgramme
	function programmesEtCompetences() {
	  return knex('programmes')
		.join('competences', 'programmes.id', 'competences.idProgramme')
		.select('programmes.id', 'nom', 'code', 'enonce');
	}

	// Fonctions d'agrégats avec regroupement sur une jointure
	// Select max(idProgramme) as idProgramme, count(code) as nbCompetences
	//     from programmes inner join competences on programmes.id = competences.idProgramme
	//     group by programmes.id
	function nbCompetencesParProgramme() {
	  return knex('programmes')
		.join('competences', 'programmes.id', 'competences.idProgramme')
		.groupBy('programmes.id')
		.max({ idProgramme: 'idProgramme' })
		.count({ nbCompetences: 'code' });
	}

**Références**  
[Site de Knex.js](http://knexjs.org/)  
[Knex cheatsheet](https://devhints.io/knex)  
