---
title: "Introduction"
weight: 2
date: 2022-01-11T14:19:27Z
draft: false
---

Git est un logiciel de gestion de versions décentralisé.  
La gestion de version, également appelé contrôle de source, désigne la pratique consistant à suivre et à gérer les changements apportés au code d'un logiciel. Les systèmes de contrôle de version sont des outils logiciels qui permettent aux équipes de développement de gérer les changements apportés au code source au fil du temps.  
Les logiciels de contrôle de version gardent une trace de chaque changement apporté au code. Si une erreur est commise, les développeurs peuvent revenir en arrière et comparer les versions antérieures du code, ce qui leur permet de corriger l'erreur tout en minimisant les perturbations pour tous les membres de l'équipe.  

**Avantages d'un système de gestion de version:**  
* Un historique complet des changements à long terme de chaque fichier est conservé. Autrement dit, le moindre changement effectué par de nombreuses personnes au fil des ans. Les changements comprennent la création et la suppression de fichiers, ainsi que l'édition de leur contenu.  
* Branchement (branching) et fusions (merges). Les membres d'une équipe pouvent travailler sur des flux de changements indépendants. La création d'une branche permet de garder plusieurs flux de travail indépendants les uns des autres. De nombreuses équipes de développement adoptent une pratique de baranchement pour chaque fonctionnalité et/ou pour chaque version. 
* Traçabilité. On peut retracer chaque changement apporté au logiciel et le connecter à des logiciels de gestion de projet et de suivi des bugs, en plus de pouvoir annoter chaque changement avec un message décrivant son but.  

